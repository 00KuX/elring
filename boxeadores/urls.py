from django.urls import path
from .views import BoxeadorListView, BoxeadorCreateView, BoxeadorUpdateView, BoxeadorDeleteView, BoxeadorDetailView
from .views import buscar, AsociacionMundialCreateView, CategoriaCreateView, CategoriaListView, CategoriaUpdateView

# BOXEADORES 

boxeadores_patterns = ([
    path('', BoxeadorListView.as_view(), name='boxeadores'),
    path('alta', BoxeadorCreateView.as_view(), name='alta'),
    path('ver/<int:pk>/', BoxeadorDetailView.as_view(), name='ver'),
    path('modifica/<int:pk>/', BoxeadorUpdateView.as_view(), name='modifica'),
    path('eliminar/<int:pk>/', BoxeadorDeleteView.as_view(), name='eliminar'),

    path('alta', AsociacionMundialCreateView.as_view(), name='alta'),

    path("buscar/", buscar, name="buscar"),

], 'boxeadores')

# ASOCIACIONES

asociaciones_patterns = ([
    path('alta', AsociacionMundialCreateView.as_view(), name='alta'),
], 'asociaciones')

# CATEGORIAS

categorias_patterns = ([
    path('', CategoriaListView.as_view(), name='categorias'),
    path('alta', CategoriaCreateView.as_view(), name='alta'),
    path('modifica/<int:pk>/', CategoriaUpdateView.as_view(), name='modifica'),
], 'categorias')