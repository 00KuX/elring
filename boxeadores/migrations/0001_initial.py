# Generated by Django 2.2.3 on 2019-07-25 23:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AsociacionContinental',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('siglas', models.CharField(max_length=5)),
                ('nombre', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='AsociacionMundial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('siglas', models.CharField(max_length=5)),
                ('nombre', models.CharField(max_length=50)),
            ],
            options={
                'verbose_name': 'Asociacion Mundial',
                'verbose_name_plural': 'Asociaciones Mundiales',
            },
        ),
        migrations.CreateModel(
            name='AsosiacionLocal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('siglas', models.CharField(max_length=5)),
                ('nombre', models.CharField(max_length=50)),
                ('anexo', models.TextField(blank=True)),
            ],
            options={
                'verbose_name': 'AsosiacionLocal',
            },
        ),
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('asoc_mundial', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boxeadores.AsociacionMundial')),
            ],
            options={
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categorias',
            },
        ),
        migrations.CreateModel(
            name='Boxeador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('apellido', models.CharField(max_length=50)),
                ('nombre', models.CharField(max_length=50)),
                ('alias', models.CharField(max_length=50)),
                ('categoria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boxeadores.Categoria')),
            ],
            options={
                'verbose_name': 'Boxeador',
                'verbose_name_plural': 'Boxeadores',
            },
        ),
        migrations.CreateModel(
            name='Boxeador_Categoria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_modificacion', models.DateTimeField(auto_now=True)),
                ('boxeador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boxeadores.Boxeador')),
                ('categoria', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boxeadores.Categoria')),
            ],
            options={
                'unique_together': {('boxeador', 'categoria')},
            },
        ),
    ]
