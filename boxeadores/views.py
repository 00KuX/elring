from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from .models import Boxeador, Categoria, ESTADO, Record, Categoria
from .models import AsociacionMundial
from .forms import BoxeadorForm, AsociacionMundialForm, CategoriaForm
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from bootstrap_modal_forms.mixins import PassRequestMixin
from combates.models import Combate
from django.db.models import Q
from django.http import JsonResponse


class BoxeadorListView(ListView):
    model = Boxeador
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Boxeadores'
        context['estados'] = ESTADO
        context['categorias'] = Categoria.objects.values()
        if self.request.GET.get('categoria', ''):
            context['categoria'] = int(self.request.GET.get('categoria', ''))
        context['texto'] = self.request.GET.get('texto', '')
        return context

    def get_queryset(self):
        boxeador = self.model.objects.all().select_related('categoria')
        categoria = self.request.GET.get('categoria', '0')
        texto = self.request.GET.get('texto', '')
        if categoria != '0':
            boxeador = boxeador.filter(categoria_id=categoria).select_related('categoria')
        if texto:
            texto = texto.upper()
            boxeador = boxeador.filter(Q(apellido__contains=texto)
                | Q(nombre__contains=texto)
                | Q(alias__contains=texto)).select_related('categoria')
        return boxeador


class BoxeadorDetailView(DetailView):
    model = Boxeador
    template_name = "boxeadores/boxeador_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Combate'
        return context
    
    def get_queryset(self):
        boxeador = super().get_queryset().select_related('categoria')
        return boxeador

    
class BoxeadorCreateView(PassRequestMixin, SuccessMessageMixin, CreateView):
    model = Boxeador
    form_class = BoxeadorForm
    template_name = "centro/agregar_modificar_modal.html"
    success_url = reverse_lazy('boxeadores:boxeadores')
    success_message = "Datos guardados exitosamente."


class BoxeadorUpdateView(PassRequestMixin, SuccessMessageMixin, UpdateView):
    model = Boxeador
    form_class = BoxeadorForm
    template_name = "centro/agregar_modificar_modal.html"
    success_url = reverse_lazy('boxeadores:boxeadores')
    success_message = "Datos guardados exitosamente."


class BoxeadorDeleteView(SuccessMessageMixin, DeleteView):
    model = Boxeador
    template_name = 'centro/eliminar_modal.html'
    success_url = reverse_lazy('boxeadores:boxeadores')
    success_message = "Datos eliminados exitosamente."

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(BoxeadorDeleteView, self).delete(request, *args, **kwargs)



class AsociacionMundialCreateView(PassRequestMixin, SuccessMessageMixin, CreateView):
    model = AsociacionMundial
    form_class = AsociacionMundialForm
    template_name = "boxeadores/asociacionmundial_form.html"
    


class CategoriaListView(ListView):
    model = Categoria
    paginate_by = 20
    



class CategoriaCreateView(PassRequestMixin, SuccessMessageMixin, CreateView):
    model = Categoria
    form_class = CategoriaForm
    template_name = "centro/agregar_modificar_modal.html"
    success_url = reverse_lazy('categorias:categorias')
    success_message = "Datos guardados exitosamente."


class CategoriaUpdateView(PassRequestMixin, SuccessMessageMixin, UpdateView):
    model = Categoria
    form_class = CategoriaForm
    template_name = "centro/agregar_modificar_modal.html"
    success_url = reverse_lazy('categorias:categorias')
    success_message = "Datos guardados exitosamente."



def buscar(request):
    term = request.GET.get("term", "")
    if term:
        if Boxeador.objects.filter(Q(nombre__contains=term.upper()) | Q(apellido__contains=term.upper())).exists():
            boxeador = Boxeador.objects.filter(Q(nombre__contains=term.upper()) | Q(apellido__contains=term.upper()))
        else:
            boxeador = []
    response = list()
    for b in boxeador:
        response.append(
            {
                "id": b.id,
                "value": "{} {} ({})".format(b.apellido, b.nombre, b.alias),
            }
        )
    return JsonResponse(response, safe=False)



# Create your views here.
