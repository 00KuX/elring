from django.db import models
from django.apps import apps
from django.db.models import Q

PROFESIONAL = "P"
AMATEUR = "A"

ESTADO = ((PROFESIONAL, "Profesional"), (AMATEUR, "Amateur"))

MALE = "M"
FEMALE = "F"

GENERO = ((MALE, "Masculino"), (FEMALE, "Femenino"))


class AsociacionMundial(models.Model):

    siglas = models.CharField(max_length=5)
    nombre = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Asociacion Mundial"
        verbose_name_plural = "Asociaciones Mundiales"

    def __str__(self):
        return "%s"  %(self.nombre)


class AsociacionContinental(models.Model):

    siglas = models.CharField(max_length=5)
    nombre = models.CharField(max_length=50)


class AsosiacionLocal(models.Model):

    siglas = models.CharField(max_length=5)
    nombre = models.CharField(max_length=50)
    anexo = models.TextField(blank=True)

    class Meta:
        verbose_name = "AsosiacionLocal"

    def __str__(self):
        pass


class Categoria(models.Model):

    nombre = models.CharField(max_length=50)
    libras = models.IntegerField(default=0)
    class Meta:
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"
        ordering = ['-libras']

    def __str__(self):
        return self.nombre

    def save(self):
        for atr in ["nombre"]:
            self.__setattr__(atr, self.__getattribute__(atr).upper())
        super().save()

    def libras_a_kilos(self):
        return "{0:.2f}".format(self.libras/2.2046)
        

class Boxeador(models.Model):

    apellido = models.CharField(max_length=50)
    nombre = models.CharField(max_length=50)
    alias = models.CharField(max_length=50)
    imagen = models.ImageField(verbose_name="Imagen", upload_to="projects")
    categoria = models.ForeignKey(
        Categoria,
        on_delete=models.CASCADE)
    genero = models.CharField(
        max_length=3, choices=GENERO, default=MALE)
    estado = models.CharField(
        max_length=3, choices=ESTADO, default=PROFESIONAL)

    class Meta:
        verbose_name = "Boxeador"
        verbose_name_plural = "Boxeadores"

    def save(self):
        for atr in ["apellido", "nombre", "alias"]:
            self.__setattr__(atr, self.__getattribute__(atr).upper())
        super().save()

    def __str__(self):
        return "%s %s (%s)" % (self.apellido, self.nombre, self.alias)

    def lista_combates(self):
        combate = apps.get_model('combates.Combate').objects.filter(
            Q(luchador_id=self.id)
            | Q(oponente_id=self.id))
        
        return combate

    def last_five_fights(self):
        return self.lista_combates().order_by('-fecha')[1:5]


class Boxeador_Categoria(models.Model):
    boxeador = models.ForeignKey(Boxeador, on_delete=models.CASCADE)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    fecha_modificacion = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("boxeador", "categoria")


class Record(models.Model):
    boxeador = models.OneToOneField(Boxeador,on_delete=models.CASCADE)
    total = models.IntegerField(default=0)
    ganado = models.IntegerField(default=0)
    ganado_ko = models.IntegerField(default=0)
    perdido = models.IntegerField(default=0)
    perdido_ko = models.IntegerField(default=0)
    empate = models.IntegerField(default=0)

    def alta(self, luchador:None, ko:None, ganador:None, empate:None):
        try:
            self = Record.objects.get(boxeador_id=luchador)
        except Record.DoesNotExist:
            self.boxeador_id = luchador
        if empate:
            self.empate = self.empate + 1
        else:
            if ganador:
                if luchador == ganador.id:
                    self.ganado = self.ganado + 1
                    if ko:
                        self.ganado_ko = self.ganado_ko + 1
                else:
                    self.perdido = self.perdido + 1
                    if ko:
                        self.perdido_ko = self.perdido_ko + 1
        self.total = self.ganado + self.perdido + self.empate
        self.save()
        

    def baja(self, luchador:None, ko:None, ganador:None, empate:None):
        self = Record.objects.get(boxeador_id=luchador.id)
        if empate:
            self.empate = self.empate - 1
        else:
            if ganador:
                if luchador.id == ganador.id:
                    self.ganado = self.ganado - 1
                    if ko:
                        self.ganado_ko = self.ganado_ko - 1
                else:
                    self.perdido = self.perdido - 1
                    if ko:
                        self.perdido_ko = self.perdido_ko - 1        
        self.total = self.ganado + self.perdido + self.empate
        self.save()

    def __str__(self):
        return "%s"  %(self.boxeador)
        

# Create your models here.
