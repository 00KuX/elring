from django import forms
from .models import Boxeador, AsociacionMundial, Categoria
from bootstrap_modal_forms.mixins import PopRequestMixin, CreateUpdateAjaxMixin


class BoxeadorForm(PopRequestMixin, CreateUpdateAjaxMixin, forms.ModelForm):

    class Meta:
        model = Boxeador
        fields = ('apellido', 'nombre', 'alias', 'categoria', 'genero', 'estado', 'imagen',)
        widgets = {
            'apellido': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'alias': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Alias'}),
            'categoria': forms.Select(attrs={"class": "form-control"}),
            'genero': forms.Select(attrs={"class": "form-control"}),
            'estado': forms.Select(attrs={"class": "form-control"}),
        }
        labels = {
            'apellido': '', 'nombre': '', 'alias': '', 'categoria': '', 'genero':'', 'estado':'',
        }

class AsociacionMundialForm(forms.ModelForm):
    class Meta:
        model = AsociacionMundial
        fields = ('siglas', 'nombre',)

class CategoriaForm(PopRequestMixin, CreateUpdateAjaxMixin, forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ('nombre', 'libras')
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'libras': forms.NumberInput(attrs={"class": "form-control"})
        }
        labels = {
            'nombre':'', 'libras':''
        }