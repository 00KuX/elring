from django.contrib import admin
from .models import Categoria, AsociacionMundial, Boxeador, Record

admin.site.register(Categoria)
admin.site.register(AsociacionMundial)
admin.site.register(Boxeador)
admin.site.register(Record)
# Register your models here.
