from django.db import models
from combates.models import Combate
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify


class Post(models.Model):
    titulo = models.CharField(max_length=150)
    slug = models.SlugField(max_length=100, unique=True)
    cuerpo = RichTextField(null=True, blank=True)
    fecha_creacion = models.DateField(auto_now_add=True)
    user_autor = models.ForeignKey(
        User, 
        verbose_name="Editor",
        on_delete=models.CASCADE
    )
    combate = models.ForeignKey(Combate, on_delete=models.CASCADE)
    publicar = models.BooleanField(blank=True, null=False, default=True)

    def __str__(self):
        return "%s" %(self.titulo)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titulo)
        super(Post, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-fecha_creacion']
