from django.urls import path
from .views import PostListView, PostCreateView, PostDetailView, PostUpdateView

blogs_patterns = ([
    path('', PostListView.as_view(), name='posts'),
    path('create/<int:combate>', PostCreateView.as_view(), name='create'),
    path('<slug:slug>/', PostDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', PostUpdateView.as_view(), name='update'),
], 'blogs')