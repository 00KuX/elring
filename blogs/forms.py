from django import forms
from .models import Post


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = (
            'titulo',
            'cuerpo',
            )

        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'cuerpo': forms.TextInput(attrs={"class": "form-control", "title": "Cargar el cuerpo del Texto"}),
        }

        labels = {
            'titulo': 'Título:',
            'cuerpo': 'Cuerpo:',
        }


class PostFormUpdate(forms.ModelForm):

    class Meta:
        model = Post
        fields = (
            'titulo',
            'cuerpo',
            )

        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'cuerpo': forms.TextInput(attrs={"class": "form-control", "title": "Cargar el cuerpo del Texto"}),
        }

        labels = {
            'titulo': 'Título:',
            'cuerpo': 'Cuerpo:',
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", "")
        super().__init__(*args, **kwargs)
            
    def clean(self):
        cleaned_data = super().clean()
        if self.user.id != None:    
            if self.user.id != self.instance.user_autor.id:
                raise forms.ValidationError(
                    u"Usted es no es propietario de este blog, no puede editar esta nota"
                )
        return cleaned_data
        



    

    