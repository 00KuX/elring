from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from .models import Post
from combates.models import Combate
from .forms import PostForm, PostFormUpdate
from django.urls import reverse_lazy
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


class PostListView(ListView):
    model = Post
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            combate = Combate.objects.get(id=self.request.GET.get('id'))
            context["combate"] = combate
        except:
            pass
        return context

    def get_queryset(self):
        queryset = self.model.objects.all()
        texto = self.request.GET.get('texto')
        if texto:
            queryset = queryset.filter(

                Q(combate__luchador__apellido__contains=texto.upper())
                | Q(combate__luchador__nombre__contains=texto.upper())
                | Q(combate__oponente__apellido__contains=texto.upper())
                | Q(combate__oponente__nombre__contains=texto.upper())
                | Q(user_autor__username__contains=texto.upper())
            )
        combate = self.request.GET.get('id')
        if combate:
            queryset = queryset.filter(combate_id=combate)
        return queryset
    

class PostDetailView(DetailView):
    model = Post
    

class PostUpdateView(UpdateView):
    model = Post
    form_class = PostFormUpdate
    success_url = reverse_lazy('blogs:posts')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super().get_form_kwargs(*args, **kwargs)
        kwargs["user"] = self.request.user
        return kwargs
 
    def form_valid(self, form):
        return super().form_valid(form)
        c = form.save(commit=False)
        c.user_autor = self.request.user
        c.combate_id = self.get_object().combate.id
        return super().form_valid(form)

        
class PostCreateView(CreateView):
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('blogs:posts')

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        c = form.save(commit=False)
        c.user_autor = self.request.user
        c.combate_id = self.kwargs.get('combate')
        return super().form_valid(form)