from django.urls import path
from .views import CombateListView, CombateCreateView, CombateDetailView, CombateUpdateView

combates_patterns = ([
    path('', CombateListView.as_view(), name='combates'),
    path('alta', CombateCreateView. as_view(), name='alta'),
    path('modifica/<int:pk>/', CombateUpdateView.as_view(), name='modifica'),
    path('ver/<int:pk>/', CombateDetailView.as_view(), name='ver'),
], 'combates')