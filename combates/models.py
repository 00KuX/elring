from django.db import models
from boxeadores.models import Boxeador, Categoria
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User


class Combate(models.Model):
    luchador = models.ForeignKey(
        Boxeador,
        related_name="Luchador",
        null=True,
        blank=True,
        on_delete=models.CASCADE)

    oponente = models.ForeignKey(
        'boxeadores.Boxeador',
        related_name="Oponente",
        null=True,
        blank=True,
        on_delete=models.CASCADE)

    fecha = models.DateTimeField()

    ganador = models.ForeignKey(
        Boxeador,
        related_name="Ganador",
        null=True,
        blank=True,
        on_delete=models.CASCADE)

    knockout = models.BooleanField(default=False)

    resumen = RichTextField(null=True, blank=True)
    empate = models.BooleanField(default=False)
    categoria = models.ForeignKey(
        Categoria,
        on_delete=models.CASCADE)

    user_create = models.ForeignKey(
        User, 
        related_name="UserCreate",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    user_update = models.ForeignKey(
        User,
        related_name="UserUpdate",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )


    class Meta:
        ordering = ['-fecha']


# Create your models here.
