from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from .forms import CombateForm
from .models import Combate
from boxeadores.models import ESTADO, Categoria, Record
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, permission_required


class CombateListView(ListView):
    model = Combate
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['estados'] = ESTADO
        context['categorias'] = Categoria.objects.values()
        context['categoria'] = int(self.request.GET.get('categoria', '0'))
        return context

    def get_queryset(self):
        estado = self.request.GET.get('estado', '')
        categoria = self.request.GET.get('categoria', '0')
        combates = super().get_queryset().select_related('categoria','ganador','luchador','oponente')
        if categoria != '0':
            combates = combates.filter(categoria_id=categoria)
            # Falta filtrar por Estado
        return combates


class CombateDetailView(DetailView):
    model = Combate

    
class CombateCreateView(SuccessMessageMixin, CreateView):
    model = Combate
    form_class = CombateForm
    success_url = reverse_lazy('combates:combates')
    success_message = "Datos guardados exitosamente."


    @method_decorator(login_required())
    @method_decorator(permission_required('combates.add_combate', reverse_lazy('home')))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Combate'
        return context

    
    def form_valid(self, form):
        c = form.save(commit=False)
        luchador = form.cleaned_data['luchador'].id
        oponente = form.cleaned_data['oponente'].id
        ganador = form.cleaned_data['ganador']
        ko = form.cleaned_data['knockout']
        empate = form.cleaned_data['empate']
        c.user_create = self.request.user
        Record().alta(luchador,ko,ganador,empate)
        Record().alta(oponente,ko, ganador, empate)
        return super().form_valid(form)


class CombateUpdateView(SuccessMessageMixin, UpdateView):
    model = Combate
    form_class = CombateForm
    success_url = reverse_lazy('combates:combates')
    success_messag = "Datos guardados exitosamente."

    @method_decorator(login_required())
    @method_decorator(permission_required('combates.add_combate', reverse_lazy('home')))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        combate = self.get_object()
        c = form.save(commit=False)
        oponente = form.cleaned_data['oponente'].id
        ganador = form.cleaned_data['ganador']
        ko = form.cleaned_data['knockout']
        luchador = form.cleaned_data['luchador'].id
        empate = form.cleaned_data['empate']
        c.user_update = self.request.user
        Record().baja(combate.luchador, combate.knockout, combate.ganador, combate.empate)
        Record().baja(combate.oponente, combate.knockout, combate.ganador, combate.empate)
        Record().alta(luchador,ko,ganador,empate)
        Record().alta(oponente,ko, ganador, empate)
        return super().form_valid(form)


# Create your views here.
