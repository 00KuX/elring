from django import forms
from .models import Combate


class CombateForm(forms.ModelForm):
    class Meta:
        model = Combate
        fields = (
            'luchador',
            'oponente',
            'categoria',
            'fecha',
            'resumen',
            'ganador',
            'knockout',
            'empate')

        widgets = {
            'luchador': forms.Select(attrs={"class": "form-control", "title": "Cargar el Luchador"}),
            'oponente': forms.Select(attrs={"class": "form-control", "title": "Cargar el Oponente"}),
            'fecha': forms.DateInput(attrs={"class": "form-control input-sm", "type": "date"},
                format="%Y-%m-%d",),
            'resumen': forms.Textarea(
                attrs={"class": "form-control input-sm"}
            ),

            'ganador': forms.Select(attrs={"class": "form-control"}),
            'categoria': forms.Select(attrs={"class": "form-control", "title": "Cargar Categoria"}),
        }

        labels = {
            'luchador': 'Luchador:',
            'oponente': 'Oponente:',
            'fecha': 'Fecha del Combate',
            'resumen': 'Resúmen:',
            'categoria': 'Categoria:',
            'ganador': 'Ganador:',
            'empate': 'Empate'
        }

    def clean(self):
        cleaned_data = super().clean()
        oponente = self.cleaned_data.get("oponente")
        luchador = self.cleaned_data.get("luchador")
        ganador = self.cleaned_data.get("ganador")
        empate = self.cleaned_data.get("empate")
        knockout = self.cleaned_data.get("knockout")
        
        if luchador == oponente:
            raise forms.ValidationError("El Oponente debe ser diferente al Luchador.")
        elif (luchador != ganador) and (oponente != ganador) and (empate is not True):
            raise forms.ValidationError("El ganador no puede ser distinto a uno de los contendientes")
        if empate and ganador:
            raise forms.ValidationError("No Puede haber un ganador y a la vez un empate, destildar el check de Empate")
        if knockout and empate:
            raise forms.ValidationError("No Puede haber un ganador por Knockout y a la vez un Empate")
        if luchador.genero != oponente.genero:
            raise forms.ValidationError("Acaba de Seleccionar Boxeadores de Distinto Sexo")
        return cleaned_data



