from django.contrib import admin
from .models import Combate


class CombateAdmin(admin.ModelAdmin):
    list_display = ('fecha',)

    class Media:
        css = {
            'all': ('combates/css/custom_ckeditor.css',)
        }

admin.site.register(Combate, CombateAdmin)

# Register your models here.
