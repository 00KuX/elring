from django.contrib import admin
from .models import Profile


class ProfileAdmin(admin.ModelAdmin):

    def user(self, obj):
        return obj.user.username

    list_display = ["user"]
    search_fields = ['user__username']

admin.site.register(Profile, ProfileAdmin)

# Register your models here.
