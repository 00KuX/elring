"""elRing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from boxeadores.urls import boxeadores_patterns, asociaciones_patterns, categorias_patterns
from combates.urls import combates_patterns
from profiles.urls import profiles_patterns
from blogs.urls import blogs_patterns
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('centro.urls')),
    path('boxeadores/', include(boxeadores_patterns)),
    path('combates/', include(combates_patterns)),
    path('profiles/', include(profiles_patterns)),
    path('asociaciones/', include(asociaciones_patterns)),
    path('categorias/', include(categorias_patterns)),
    path('blogs/', include(blogs_patterns)),
    

    # Autenticacion
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('registration.urls')),
]


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    