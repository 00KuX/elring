**El Ring**

Portal de Deportes de contacto.

Combates - Estadisticas - Blog

**Como Iniciar**


Clonar repositorio

1. Crear entorno virtual $ virtualenv --python=pthon3.6 env
1. Iniciar entorno virtual $ source env/bin/activate
1. Instalar requirements $ pip install -r requirements.txt
1. Crear database $ python manage.py migrate
1. Iniciar servidor $ python manage.py runserver
