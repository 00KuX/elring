from django.views.generic.base import TemplateView
from combates.models import Combate


class HomePageView(TemplateView):
    template_name = "centro/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['combates'] = Combate.objects.all().select_related('luchador','oponente','ganador','categoria')
        context['titulo'] = "Home"
        return context