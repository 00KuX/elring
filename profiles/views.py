from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from registration.models import Profile
from django.db.models import Q


class ProfileListView(ListView):
    model = Profile
    paginate_by = 20
    template_name = "profiles/profile_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Perfiles - Editores'
        return context

    def get_queryset(self):
        profiles = super().get_queryset()
        if self.request.GET.get('texto',''):
            profiles = super().get_queryset().filter(
                Q(user__username__contains=self.request.GET.get('texto',''))|
                Q(user__first_name__contains=self.request.GET.get('texto',''))|
                Q(user__last_name__contains=self.request.GET.get('texto','')))
        return profiles


class ProfileDetailView(DetailView):
    model = Profile
    template_name = "profiles/profile_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Perfiles - Editores'
        return context

    def get_object(self):
        return get_object_or_404(Profile, user__username=self.kwargs['username'])
